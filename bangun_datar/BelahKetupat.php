<?php
 
    class BelahKetupat extends BangunDatar
    {
        function keliling($data)
        {
            return 4 * $data[0];
        }

        function luas($data)
        {
            return ($data[0] * $data[1])/2;
        }
    }
?>
    
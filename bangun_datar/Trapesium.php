<?php
    
    class Trapesium extends BangunDatar
    {
        function keliling($data)
        {
            return $data[0]+ $data[1] + $data[2] + $data[3];
        }

        function luas($data)
        {
            return (($data[0] + $data[1])*$data[2])/2;
        }
    }
?>
    
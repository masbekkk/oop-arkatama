<?php
    class Lingkaran extends BangunDatar
    {
        var $phi = 3.14;

        function keliling($data)
        {
            return 2 * $this->phi * $data[0];
        }

        function luas($data)
        {
            return $this->phi * ($data[0] * $data[0]);
        }
    }
?>
    
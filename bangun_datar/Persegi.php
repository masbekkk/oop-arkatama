<?php
 
    class Persegi extends BangunDatar
    {
        function keliling($data)
        { 
            return 4*$data;
        }

        function luas($p)
        {
            return $p * $p;
        }
    }
?>
    
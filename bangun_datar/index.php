<?php
    // error_reporting(0);
    include('./BangunDatar.php');
    include('./Persegi.php'); 
    include('./PersegiPanjang.php'); 
    include('./Segitiga.php'); 
    include('./Lingkaran.php'); 

    include('./BelahKetupat.php');
    include('./Trapesium.php');
    include('./JajarGenjang.php');


    $panjang = 5; $lebar = 10; $tinggi = 8; $d1 = 10; $d2 = 10;
    $a = 4; $b = 6; $c =  7; $d = 8;
    
    $persegi = new Persegi();
    echo "Keliling persegi dgn sisi $panjang =  {$persegi->keliling($panjang)}<br>";
    echo "Luas persegi dgn sisi $panjang =  {$persegi->luas($panjang)}<br>";

    $PersegiPanjang = new PersegiPanjang();
    echo "Keliling PersegiPanjang dgn p : $panjang dan L : $lebar =  {$PersegiPanjang->keliling([$panjang,$lebar])}<br>";
    echo "Luas PersegiPanjang dgn p = $panjang dan L : $lebar =  {$PersegiPanjang->luas([$panjang,$lebar])}<br>";

    $Segitiga = new Segitiga();
    echo "Keliling Segitiga dgn p : $panjang dan L : $lebar dan tinggi : $tinggi =  {$Segitiga->keliling([$panjang, $lebar, $tinggi])}<br>";
    echo "Luas Segitiga dgn = $panjang dan L : $lebar dan tinggi : $tinggi =  {$Segitiga->luas([$panjang, $lebar])}<br>";

    $Lingkaran = new Lingkaran();
    echo "Keliling Lingkaran dgn jari-jari $panjang =  {$Lingkaran->keliling([$panjang])}<br>";
    echo "Luas Lingkaran dgn jari-jari $panjang =  {$Lingkaran->luas([$panjang])}<br>";


    $trapesium = new Trapesium();
    echo "Keliling trapesium dgn pjg masing-masing sisi $a, $b, $c, $d  =  {$trapesium->keliling([$a, $b, $c, $d])}<br>";
    echo "Luas Trapesium dgn = $panjang dan L : $lebar dan tinggi : $tinggi =  {$trapesium->luas([$panjang, $lebar, $tinggi])}<br>";

    $JajarGenjang = new JajarGenjang();
     echo "Keliling JajarGenjang dgn p : $panjang dan L : $lebar =  {$JajarGenjang->keliling([$panjang, $lebar])}<br>";
    echo "Luas JajarGenjang dgn p = $panjang dan L : $lebar =  {$JajarGenjang->luas([$panjang, $lebar])}<br>";

    $BelahKetupat = new BelahKetupat();
    echo "Keliling belahketupat dgn sisi $panjang =  {$BelahKetupat->keliling([$panjang])}<br>";
    echo "Luas Belahketupat dgn d1 = $d1 dan d2 : $d2 =  {$BelahKetupat->luas([$d1,$d2])}<br>";

?>
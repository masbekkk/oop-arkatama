<?php
 
    class PersegiPanjang extends BangunDatar
    {
        function keliling($data)
        {
            return ($data[0]+$data[1]) * 2;
        }

        function luas($data)
        {
            return $data[0] * $data[1];
        }
    }
?>
    
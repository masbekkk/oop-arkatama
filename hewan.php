<?php
    //Tugas awal
    // class Hewan{
    //     var $nama, $jumlah_kaki, $jenis;

    //     function __construct($name, $jml_kaki, $jns){
    //         $this->nama = $name;
    //         $this->jumlah_kaki = $jml_kaki;
    //         $this->jenis = $jns;
    //     }

    //     function bunyiHewan($bunyi) {
    //         return $bunyi;
    //     }
    // }

    class Hewan{
        var $numberOfLegs;
        var $canFly;
        var $foodClassification;
        var $canSwim;
        var $respirationOrgan;
        var $nama;

        function eat(){ return "Lagi makan";}        
        function run(){ return "Lagi lari"; }
        function swim(){ 
            if($this->canSwim)return "Bisa renang";
            else return "Gabisa renang";
        }
        function fly(){ 
            if($this->canFly)return "Bisa terbang";
            else return "Gabisa terbang";
        }
        function cry(){ return "Lagi nangis";}

        function __construct($nama, $jml_kaki, $bisa_fly, $jenis_food, $bisa_swim, $organ_nafas){
            $this->nama = $nama;
            $this->numberOfLegs = $jml_kaki;
            $this->canFly = $bisa_fly;
            $this->foodClassification = $jenis_food;
            $this->canSwim = $bisa_swim;
            $this->respirationOrgan = $organ_nafas;
        }

        function isi(){
            echo "<b>Nama Hewan: {$this->nama}</b>, <br>Jumlah kaki: {$this->numberOfLegs}, <br>Terbang: {$this->fly()}, <br>Jenis Makanan: {$this->foodClassification}, <br>Renang: {$this->swim()}, <br>Organ Nafas: {$this->respirationOrgan}, <br>Makan: {$this->eat()}, <br>Lari: {$this->run()}, <br>Nangis: {$this->cry()}<br><hr>";
        }
        
    }

    class CatFish extends Hewan{

    }

    class BettaFish extends Hewan{

    }

    class Crocodile extends Hewan{

    }

    class Aligator extends Hewan{

    }

    class Lizard extends Hewan{

    }

    class Snake extends Hewan{

    }

    class Swan extends Hewan{

    }

    class Chicken extends Hewan{

    }

    class Duck extends Hewan{

    }

    class Human extends Hewan{

    }

    class Whale extends Hewan{

    }

    class Dolphin extends Hewan{

    }

    class Bat extends Hewan{

    }

    class Tiger extends Hewan{

    }

?>

